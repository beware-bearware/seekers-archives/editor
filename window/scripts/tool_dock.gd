# This is where tools are put when an editor is swapped to.
extends VBoxContainer

var editor_button_group = {}



func add_editor_tool(
	connected_editor: BaseEditor, 
	target_tool: BaseTool, 
	tool_icon: ImageTexture, 
	tool_name: String
) -> void:
	# This can and should all be replaced with loading a scene.
	# This is scrictly not needed.
	var tool_button: Button = Button.new()
	tool_button.set_script(load("res://common/scripts/tool_button.gd"))
	tool_button.connected_tool = target_tool
	connected_editor.connect("tool_switched", tool_button.update_tool_selection)
	tool_button.icon = tool_icon
	tool_button.icon_alignment = HORIZONTAL_ALIGNMENT_CENTER
	tool_button.vertical_icon_alignment = VERTICAL_ALIGNMENT_CENTER
	tool_button.expand_icon = true
	tool_button.name = tool_name
	tool_button.toggle_mode = true
	tool_button.custom_minimum_size = Vector2i(64, 64)
	# This is where the real logic is. Simply tracking button groups
	# for each editor.
	if connected_editor not in editor_button_group.keys():
		var new_button_group = ButtonGroup.new()
		editor_button_group[connected_editor] = new_button_group
	tool_button.button_group = editor_button_group[connected_editor]
	self.add_child(tool_button)
	return
