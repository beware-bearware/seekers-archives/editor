# The cut tool is used to manipulate the layout of a given sheet
# by dividing sections in specific "depths" into specific zones
# that are used for orienting data presentation.
# Depth is defined here as the amount of parent nodes between
# a given node and the root node of the sheet layout.
# Nodes right under the root layout for instance is depth 1,
# and ALL of their children are at depth 2 and so on.
extends BaseTool

const ORIENTATION_VERTICAL: int = 0
const ORIENTATION_HORIZONTAL: int = 1

var current_depth: int = 1
var operation_preview: Control
var depth_preview: Control
var hovered_node: Control



func _ready():
	operation_preview = $OperationPreview
	depth_preview = $DepthPreview
	return


# Setting depth means setting up the previews here.
func set_depth(target_depth: int) -> void:
	var depth_nodes: Array[Node] = get_nodes_at_depth(target_depth)
	current_depth = target_depth
	# Remove any other depth that was already there.
	for child in depth_preview.get_children():
		if child.name != "CutPreview":
			child.get_parent().remove_child(child)
	
	# Create a mirror of all the nodes at the depth using the operation buttons
	# that can update the cut preview.
	for node in depth_nodes:
			if node is BoxContainer or node is PanelContainer:
				var preview_node: Panel = Panel.new()
				preview_node.global_position = node.global_position
				preview_node.size = node.size
				#preview_node.set_script(load("res://editors/sheet/tool_cut/operation_preview_button.gd"))
				depth_preview.add_child(preview_node)


# Depth (defined above) should get all nodes even spread across
# different parents.
func get_nodes_at_depth(target_depth: int) -> Array[Node]:
	var sheet: Sprite2D = $"../../Sheet" # Sheet preview
	var layout_root = sheet.get_node("LayoutRoot")
	var depth_nodes: Array[Node] = [layout_root]
	
	for current_depth in range(0, target_depth):
		var depth_nodes_iterate: Array[Node] = depth_nodes
		depth_nodes = []
		for node in depth_nodes_iterate:
			for child in node.get_children():
				depth_nodes.append(child)
	return depth_nodes


# Translate the preview to the actual page layout. This is where
# the fun begins.
func apply_operation() -> void:
	if not hovered_node: # for off-bounds clicks.
		return
	var target_orientation: int = current_rotation % 2
	var cut_node: Control = hovered_node
	var cut_parent: Control = hovered_node.get_parent()
	
	# Non box, or boxes oriented the wrong way, must be thrown down to the next depth
	# in order for this cut to workout.
	if cut_node is PanelContainer or \
	(target_orientation == ORIENTATION_HORIZONTAL and cut_node is VBoxContainer) or \
	(target_orientation == ORIENTATION_VERTICAL and cut_node is HBoxContainer):
		var new_box: BoxContainer
		match target_orientation:
			ORIENTATION_HORIZONTAL: new_box = HBoxContainer.new()
			ORIENTATION_VERTICAL  : new_box = VBoxContainer.new()
		new_box.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		new_box.size_flags_vertical = Control.SIZE_EXPAND_FILL
		cut_parent.remove_child(cut_node)
		cut_parent.add_child(new_box)
		new_box.add_child(cut_node)
		cut_node = new_box
	
	# For sure at this point, we can just add the child in and move it to the right spot.
	var new_node: PanelContainer = PanelContainer.new()
	new_node.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	new_node.size_flags_vertical = Control.SIZE_EXPAND_FILL
	cut_node.add_child(new_node)
	if operation_preview.is_cut_fill_first_child():
		cut_node.move_child(new_node, 0)
	return


# The tool menu for the cut tool so far is just depth configuration.
func show_tool_menu() -> void:
	set_depth(current_depth)
	var depth_popup: Popup = $DepthMenu
	depth_popup.visible = true
	# Offset just a bit
	depth_popup.position = $/root.get_viewport().get_mouse_position()
	depth_popup.position += Vector2i(20, 10)
	# Keep the popup in bounds
	if depth_popup.position.x < 0:
		depth_popup.position.x = 0
	if depth_popup.position.y < 0:
		depth_popup.position.y = 0
	if depth_popup.position.x + depth_popup.size.x > get_viewport().size.x:
		depth_popup.position.x = get_viewport().size.x - depth_popup.size.x
	if depth_popup.position.y + depth_popup.size.y > get_viewport().size.y:
		depth_popup.position.y = get_viewport().size.y - depth_popup.size.y
	
	# Find the amount of depths known.
	var current_scan_depth: int = 0
	var nodes_at_depth: Array[Control] = [$"../../Sheet/LayoutRoot"]
	var nodes_at_next_depth: Array[Control] = []
	var bottom_reached: bool = false
	while not bottom_reached:
		nodes_at_next_depth = []
		for node in nodes_at_depth:
			for child in node.get_children():
				nodes_at_next_depth.append(child)
		nodes_at_depth = nodes_at_next_depth
		bottom_reached = (nodes_at_next_depth.size() == 0)
		current_scan_depth += 1
	
	# Clear other existing depth info.
	for child in $DepthMenu/Organizer/DepthSelectionScroller/DepthSelection.get_children():
		child.get_parent().remove_child(child)
	
	# Fill the menu with depth enteries.
	for depth_index in range(0, current_scan_depth - 1):
		var depth_entry = load("res://editors/sheet/tool_cut/scenes/depth_entry.tscn").instantiate()
		depth_entry.depth = depth_index + 1
		depth_entry.cut_tool = self
		$DepthMenu/Organizer/DepthSelectionScroller/DepthSelection.add_child(depth_entry)
	return


func _selected():
	set_depth(current_depth)
	$"../../Sheet/LayoutRoot".set_theme(load("res://common/themes/faint_layout.tres"))
	return


func _unselected():
	$"../../Sheet/LayoutRoot".set_theme(load("res://common/themes/invisible_layout.tres"))
	return


func _tool_process(_delta):
	if $DepthMenu.visible: operation_preview.visible = false
	else: operation_preview.visible = true
	
	var hover_position: Vector2 = get_global_mouse_position()
	var hover_node_found: bool = false
	for node in get_nodes_at_depth(current_depth):
		var node_top_bound:    int = node.global_position.y
		var node_right_bound:  int = node.global_position.x + node.size.x
		var node_bottom_bound: int = node.global_position.y + node.size.y
		var node_left_bound:   int = node.global_position.x
		if hover_position.x >= node_left_bound and hover_position.x < node_right_bound and \
		hover_position.y >= node_top_bound and hover_position.y < node_bottom_bound:
			hovered_node = node
			hover_node_found = true
			break
	if not hover_node_found: hovered_node = null
	
	operation_preview.update_cut_preview()
