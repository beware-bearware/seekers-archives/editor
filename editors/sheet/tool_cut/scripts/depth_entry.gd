extends Control

var cut_tool: BaseTool
var depth_nodes: Array[Node]
var depth: int = 1



# Getting full list of nodes at a speicfic depth level
# of the layout not heigherarchy, required by the cutting
# tool to  let the user specify which level of layout
# to divide into new parts.
func _ready():
	$Organizer/LevelDisplay/DepthLabel.text = str(depth)
	depth_nodes = cut_tool.get_nodes_at_depth(depth - 1)
	return
