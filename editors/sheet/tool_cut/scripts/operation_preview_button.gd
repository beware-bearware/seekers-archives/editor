extends Button

var cut_displayed: bool = false
var operation_preview: Control



func _ready():
	operation_preview = $".."
	return


# Handle previews for hovered splittables.
func _process(_delta):
	if cut_displayed and not is_hovered():
		operation_preview.unrequest_cut_preview(self)
		cut_displayed = false
	if is_hovered() and not cut_displayed:
		operation_preview.request_cut_preview(self)
		cut_displayed = true
	if is_hovered() and cut_displayed:
		return
