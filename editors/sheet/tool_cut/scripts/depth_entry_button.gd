extends Button

# Keeping track for clean up purposes.
var previews_displayed: bool = false
var preview_nodes: Array[Panel] = []
var original_depth: int = 0
# Will need this for depth changing.
var cut_tool: BaseTool
var depth_entry: Control
var depth_preview: Control



func _ready():
	depth_entry = $".."
	cut_tool = depth_entry.cut_tool
	depth_preview = cut_tool.get_node("DepthPreview")
	return


func _pressed():
	cut_tool.set_depth(depth_entry.depth)
	original_depth = depth_entry.depth
	return


# This is all just for showing a live depth
# preview for the user while they hover over
# the depth options in the tooltip.
func _process(_delta):
	# Create a preview
	if is_hovered() and not previews_displayed:
		# Copy every node location to an empty
		# panel for quick display and make sure
		# to leave the sheet ALONE.
		original_depth = cut_tool.current_depth
		cut_tool.set_depth(depth_entry.depth)
		previews_displayed = true
	
	# Clean up a preview
	if not is_hovered() and previews_displayed:
		cut_tool.set_depth(original_depth)
	return
