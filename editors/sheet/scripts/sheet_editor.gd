# This is the editor for specifically making and modifying stat sheets
# that will be seen by things like players. This can be for character sheets
# mainly, but can also very well be things like handouts or NPC sheets if 
# the system designer sees it fit to do so.
extends BaseEditor



# Register all the tools and get them setup.
func _ready():
	register_tool(self, $Tools/Select, "res://common/icons/cursor.svg", "Select")
	register_tool(self, $Tools/Cut, "res://common/icons/knife_blocks.svg", "Cut")
	register_tool(self, $Tools/Push, "res://common/icons/border_push.svg", "Push")
	register_tool(self, $Tools/Move, "res://common/icons/4_cursor.svg", "Move")
	get_tools()[0].select()
	tool_switched.emit(get_tool())
