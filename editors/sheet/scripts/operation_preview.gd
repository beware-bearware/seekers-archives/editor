# This is the visual part of the cut tool
# in terms of the operation, or the "cut"
# in question.
extends Control

const ALIGNED_TOP: int = 0
const ALIGNED_RIGHT: int = 1
const ALIGNED_BOTTOM: int = 2
const ALIGNED_LEFT: int = 3

var cut_preview: BoxContainer
var cut_preview_requestor: Button



func get_cut_fill() -> Vector4:
	var fill: Panel = $NewNode
	var fill_pos: Vector2 = fill.global_position
	var fill_size: Vector2 = fill.size
	return Vector4(fill_pos.x, fill_pos.y, fill_size.x, fill_size.y)


# first child not as in the child of the operation preview,
# but as in "is the node that will be created by the cut
# going to be before the other siblings it will be added to"
func is_cut_fill_first_child() -> bool:
	var alignment: int = get_parent().current_rotation % 4
	if alignment == ALIGNED_BOTTOM or alignment == ALIGNED_LEFT:
		return true
	else:
		return false


# position the two preview nodes.
func update_cut_preview():
	var alignment: int = get_parent().current_rotation % 4
	
	var top_node: Panel
	var bottom_node: Panel
	# "old" means where previous children should go.
	# "new" means the created empty space.
	if alignment == ALIGNED_TOP or alignment == ALIGNED_RIGHT:
		top_node = $OldNode
		bottom_node = $NewNode
	else:
		top_node = $NewNode
		bottom_node = $OldNode
	var node_to_preview: Control = get_parent().hovered_node
	
	if not node_to_preview:
		top_node.visible = false
		bottom_node.visible = false
		return
	top_node.visible = true
	bottom_node.visible = true
	
	# Apply mirror to target node, while showing the cut. This uses the alignment
	# mask function defined lower down to not have a bunch of repeated code for
	# only effecting specific axis based on rotation.
	var mask_position: Vector2 = node_to_preview.global_position
	var mask_size: Vector2 = node_to_preview.size
	var node_mouse_position: Vector2 = get_global_mouse_position() - mask_position
	
	top_node.position    = mask_position
	top_node.size        = alignment_mask(node_mouse_position, mask_size, alignment)
	bottom_node.position = alignment_mask(mask_position + top_node.size, mask_position, alignment)
	bottom_node.size     = alignment_mask(mask_size - top_node.size, mask_size, alignment)


# Takes the base vector, and replaces either the x or y value with the x or y
# from the mask, based entirely on the alignment being horizontal or vertical.
# This helps make the above code less verbose.
func alignment_mask(base: Vector2, mask: Vector2, alignment: int) -> Vector2:
	# 0 is vertical/y and 1 is horizontal/x for the axis.
	var axis: int = alignment % 2
	var off_axis: int = (alignment+1) % 2
	var axis_filter: Vector2 = Vector2(axis, off_axis)
	var off_axis_filter: Vector2 = Vector2(off_axis, axis)
	var filtered_mask: Vector2 = mask * off_axis_filter
	return (base * axis_filter) + filtered_mask
