# this is the visual extention of the Push tool.
# It exists pretty much entirley as a visual feature
# and does not do anything to change state of anything.
extends Panel

const ALIGNED_TOP: int = 0
const ALIGNED_RIGHT: int = 1
const ALIGNED_BOTTOM: int = 2
const ALIGNED_LEFT: int = 3

var push_threshold: int
var highlighter: StyleBoxFlat
var current_alignment: int = -1

func _ready():
	push_threshold = get_parent().PUSH_THRESHOLD
	highlighter = get_theme_stylebox("panel")


func highlight_alignment(alignment: int) -> void:
	if alignment == current_alignment:
		return
	current_alignment = alignment
	var new_highlighter: StyleBoxFlat = highlighter.duplicate()
	match alignment:
		ALIGNED_TOP:    new_highlighter.border_width_top    = push_threshold
		ALIGNED_RIGHT:  new_highlighter.border_width_right  = push_threshold
		ALIGNED_BOTTOM: new_highlighter.border_width_bottom = push_threshold
		ALIGNED_LEFT:   new_highlighter.border_width_left   = push_threshold
	remove_theme_stylebox_override("panel")
	add_theme_stylebox_override("panel", new_highlighter)
	return

func highlight_node(target_node: Control) -> void:
	self.global_position = target_node.global_position
	self.size = target_node.size
	return
