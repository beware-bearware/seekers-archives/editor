extends BaseTool

# Alignment in this case refers to the side
# of a given node. This mostly referes to
# push zones.
const ALIGNED_TOP: int = 0
const ALIGNED_RIGHT: int = 1
const ALIGNED_BOTTOM: int = 2
const ALIGNED_LEFT: int = 3

# How much space from a node boundary you can
# be for a push zone to find your cursor.
const PUSH_THRESHOLD: int = 30

var push_zones: Array[PushZone] = []
var last_hovered_push_zone: PushZone
var push_highlighter: Panel # child node.
var still_operating: bool = false # this is to check for activity in previous frame.



func _ready():
	push_highlighter = $PushHighlighter
	return


# On select: get a list of valid PushZones, since the layout of the 
# sheet should otherwise be static.
func _selected():
	var layout_root: VBoxContainer = $"../../Sheet/LayoutRoot"
	layout_root.set_theme(load("res://common/themes/faint_layout.tres"))
	
	# go through all the nodes in the layout and record their 4 push zones.
	var current_node_set: Array[Control] = [layout_root]
	var next_node_set: Array[Control] = []
	var potential_push_zones: Array[PushZone] = []
	while current_node_set.size() > 0:
		next_node_set = []
		for node in current_node_set:
			for child in node.get_children():
				next_node_set.append(child)
				# skip getting push zones for node
				# if it has no siblings to push.
				if node.get_child_count() < 2:
					continue
				# A zone for each alignment! Now
				# for the low price of "free"!
				for alignment in range(0,4):
					var push_zone = PushZone.new()
					push_zone.set_connected_node(child)
					push_zone.set_alignment(alignment)
					potential_push_zones.append(push_zone)
		current_node_set = next_node_set
	
	# Isolated zones are zones with no matching zone on the
	# other side. Think sides of the page, which shouldn't
	# be pushable.
	push_zones = remove_isolated_zones(potential_push_zones)
	return


func _unselected():
	$"../../Sheet/LayoutRoot".set_theme(load("res://common/themes/invisible_layout.tres"))
	return


func _tool_process(_delta):
	var current_push_zone: PushZone
	# Don't find a new one if still dragging
	if still_operating:
		current_push_zone = last_hovered_push_zone
	else:
		# find a zone. Only set the variable if one is found.
		for push_zone in push_zones:
			var bounds: Vector4 = push_zone.get_bounds()
			var mouse_position: Vector2 = get_global_mouse_position()
			if mouse_position.x > bounds.x and mouse_position.y > bounds.y and \
			mouse_position.x < bounds.z and mouse_position.y < bounds.w:
				current_push_zone = push_zone
				break
	
	if not current_push_zone:
		# we know that a node is not being dragged,
		# and that no zone is hovered.
		push_highlighter.visible = false
		return
	else:
		# just update the highlighter node.
		push_highlighter.highlight_node(current_push_zone.get_connected_node())
		var alignment: int = current_push_zone.get_alignment()
		push_highlighter.highlight_alignment(alignment)
		push_highlighter.visible = true
	
	# This gets unset when a push zone is no longer being used.
	# it is used to override new node selection if the mouse
	# leaves the push zone. (See above)
	if not last_hovered_push_zone:
		last_hovered_push_zone = current_push_zone
	
	# reset this, see last comment.
	if last_hovered_push_zone != current_push_zone:
		last_hovered_push_zone = null
	
	still_operating = is_applying_operation
	
	var pushed_node: Control = current_push_zone.get_connected_node()
	if is_applying_operation:
		apply_push_zone(current_push_zone)


# This is the real meat and potatos, this is responsible
# for updating both the layout and the preview to the new
# ratio. Explicitly the ratio. Works better for things
# like web editors or other godot projects to use
# relative spacing and not specific pixel count.
func apply_push_zone(target_push_zone: PushZone) -> void:
	# setup for these, they are used all over the place.
	var alignment:int = target_push_zone.get_alignment()
	var pushed_node: Control = target_push_zone.get_connected_node()
	var pushed_node_parent: Control = pushed_node.get_parent()
	# siblings here includes the pushed_node.
	var pushed_node_siblings: Array[Node] = pushed_node_parent.get_children()
	
	# find the total sizes between all sibling nodes including those not pushed.
	# Why? Because having a ratio of 1200:1100 is dumb and should be simplified
	# to a common factor, just cleaner that way. Also helps with snapping,
	# which will be added in a tool menu after the cut tool is made to not be
	# as buggy.
	var sizes: Array[float] = []
	var total_size: float = 0
	for child in pushed_node_siblings:
			var filtered_size: float = alignment_filter_vector2(child.size, alignment)
			total_size += filtered_size
			sizes.append(filtered_size)
	
	# Getting the distance between where the node bound is, and where it wants to be.
	var vector_target_push_distance: Vector2
	var push_bound_position: Vector2
	vector_target_push_distance = (pushed_node.size + pushed_node.global_position) - get_global_mouse_position()
	push_bound_position = pushed_node.global_position
	if alignment == ALIGNED_LEFT or alignment == ALIGNED_TOP:
		vector_target_push_distance -= pushed_node.size
	var target_push_distance: float = alignment_filter_vector2(vector_target_push_distance, alignment)
	
	# Getting sibling info
	var pushed_sibling: Control
	var pushed_sibling_position: int = alignment_filter_vector4(Vector4(-1, 1, 1, -1), alignment)
	var pushed_node_index: int = pushed_node_siblings.find(pushed_node)
	pushed_sibling = pushed_node_parent.get_child(pushed_node_index + pushed_sibling_position)
	# change target distance based on the sibling's position relative to the pushed node.
	target_push_distance = target_push_distance * -1 * pushed_sibling_position
	
	# apply new sizings to node and its sibling
	sizes[pushed_node_siblings.find(pushed_node)] += target_push_distance
	sizes[pushed_node_siblings.find(pushed_sibling)] -= target_push_distance
	for child in pushed_node_siblings:
		child.size_flags_stretch_ratio = sizes[pushed_node_siblings.find(child)]


# Take an array of push zones and give back only push zones with
# a valid neighbor (another push zone which shares its bound)
func remove_isolated_zones(all_push_zones: Array[PushZone]) -> Array[PushZone]:
	var returnable_push_zones: Array[PushZone] = []
	# "edge" in this instance is a vector defined in the following way:
	# the first number, "x" is the alignment of the edge, a number tied
	# to the alignment constants.
	# the second number, "y" is the position of the edge on it's axis.
	# a top or bottom aligned edge for example, has it's second number
	# represent the y coordinate on which the edge falls.
	var edge_positions: Array[Vector2i] = []
	var past_push_zones: Array[PushZone] = []
	for push_zone in all_push_zones:
		var zone_bounds: Vector4 = push_zone.get_bounds()
		var edge_position: Vector2i = Vector2i(0, 0)
		edge_position.x = push_zone.get_alignment()
		var desired_edge: Vector2i = Vector2i(0, 0)
		# assign flipped alignment
		desired_edge.x = (edge_position.x + 2) % 4
		# find the coordinate for the edge vector
		var zb: Vector4 = zone_bounds # shorter name for next line
		var edge_coords: Vector4 = Vector4(zb.y, zb.z, zb.w, zb.x)
		var edge_coord: int = alignment_filter_vector4(edge_coords, edge_position.x)
		
		edge_position.y = edge_coord
		desired_edge.y = edge_coord
		edge_positions.append(edge_position)
		past_push_zones.append(push_zone)
		
		# if the desired edge has been recorded in the list of all edges,
		# we must have a non-isolated pair of edges, and thus must return
		# the push zone, and not the edge.
		if desired_edge in edge_positions:
			returnable_push_zones.append(push_zone)
			var matching_edge_index: int = edge_positions.find(desired_edge)
			returnable_push_zones.append(past_push_zones[matching_edge_index])
	return returnable_push_zones


# Vector2 can pass conditional values, which are picked based on alignment.
# Vector2 filter simplifies to x being horizontal and y being vertical.
func alignment_filter_vector2(target_vector: Vector2, alignment: int) -> float:
	if alignment == ALIGNED_TOP or alignment == ALIGNED_BOTTOM:
		return target_vector.y
	else:
		return target_vector.x


# Vector4 can pass conditional values, which are picked based on alignment.
func alignment_filter_vector4(target_vector: Vector4, alignment: int) -> float:
	match alignment % 4:
		ALIGNED_TOP:    return target_vector.x
		ALIGNED_RIGHT:  return target_vector.y
		ALIGNED_BOTTOM: return target_vector.z
		ALIGNED_LEFT:   return target_vector.w
		_: return target_vector.x



# PushZone class, represents a "handle" or zone in which the edge
# of a node may be selected in order to "push" or resize it.
# This is referred to as pushing instead of resize as it changes
# the ratios of the stretch flags, instead of the overall size.
# The alignment is the node edge. A zone at the bottom of its
# node has the bottom alignment for instance.
# Additionally, they store the node and can calculate the bounds
# they hold.

class PushZone:
	var connected_node: Control
	var alignment: int


	func set_connected_node(target_node: Control) -> void:
		connected_node = target_node


	func set_alignment(target_alignment: int) -> void:
		alignment = target_alignment % 4


	func get_connected_node() -> Control:
		return connected_node


	func get_alignment() -> int:
		return alignment


	# the "bounds" are the top left corner stored as x -> x and y -> y, and
	# the bottom right corner stored as x -> z and y -> w. This is because
	# this information is all thats needed to show the bounds of where
	# the zone lies.
	func get_bounds() -> Vector4:
		var push_zone: Vector4 = Vector4(0, 0, 0, 0)
		var lower_corner: Vector2 = connected_node.global_position
		var higher_corner: Vector2 = lower_corner
		
		match alignment:
			ALIGNED_TOP: 
				higher_corner += Vector2(connected_node.size.x, PUSH_THRESHOLD)
			ALIGNED_RIGHT:
				lower_corner.x += connected_node.size.x - PUSH_THRESHOLD
				higher_corner += connected_node.size
			ALIGNED_BOTTOM:
				lower_corner.y += connected_node.size.y - PUSH_THRESHOLD
				higher_corner += connected_node.size
			ALIGNED_LEFT: 
				higher_corner += Vector2(PUSH_THRESHOLD, connected_node.size.y)
		
		push_zone.x = lower_corner.x
		push_zone.y = lower_corner.y
		push_zone.z = higher_corner.x
		push_zone.w = higher_corner.y

		return push_zone
