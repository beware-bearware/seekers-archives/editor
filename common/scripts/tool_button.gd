# This is put on the buttons in the tool dock that allows for
# changing tools graphically.
extends Button

var connected_tool: BaseTool



func _pressed():
	connected_tool.select()
	return


# In cases that a tool is selected *outside* of having
# a button pressed, the tool buttons will not reflect
# the current tool being used. To solve this, any logic
# attempting to change a tool without using the buttons
# should make sure that they run this as well.
func update_tool_selection(tool: BaseTool) -> void:
	if tool == connected_tool:
		button_pressed = true
		return
	button_pressed = false
