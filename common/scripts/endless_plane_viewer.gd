# Endless Planes in this instance are the 2d scrollable planes that
# can be seen in some of the editors, like the character sheet or
# any node based editor.
extends Camera2D



func _input(event):
	if event is InputEventMouseMotion and Input.is_action_pressed("camera_drag"):
		self.position -= event.relative / self.zoom
	
	if event.is_action("zoom_in") and self.zoom.y < 10:
		self.zoom *= Vector2(1.1, 1.1)
	if event.is_action("zoom_out") and self.zoom.y > 0.1:
		self.zoom *= Vector2(0.9, 0.9)
