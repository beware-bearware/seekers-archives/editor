# This is the class used by the unique editor types,
# stuff like the character sheet editor, catalog editor
# book editor, all of it is an extention of this class.
class_name BaseEditor extends Control

signal tool_switched(new_tool: BaseTool)

var current_tool: BaseTool
# This is to track all tools the editor 
# is responsible for.
var known_tools: Array[BaseTool]



func new() -> BaseEditor:
	known_tools = []
	return self


func get_tools() -> Array[BaseTool]:
	return known_tools


func get_tool() -> BaseTool:
	return current_tool


# Registering lets a tool be known by the editor,
# and show up on the side tool dock.
func register_tool(
	connected_editor: BaseEditor, 
	target_tool: BaseTool, 
	icon_path: String, 
	tool_name: String
) -> void:
	known_tools.append(target_tool)
	var icon: ImageTexture = ImageTexture.create_from_image(Image.load_from_file(icon_path))
	$/root/Main/ActiveView/ToolDock.add_editor_tool(connected_editor, target_tool, icon, tool_name)
	target_tool._register_editor(self)
	return


# This is called by a tool the editor knows. It shouldn't be
# called by anything else. Changing tools should be done
# by getting the tool and using its select() method instead.
func _set_tool(target_tool: BaseTool) -> void:
	if current_tool:
		current_tool._unselected()
	target_tool._selected()
	current_tool = target_tool
	for tool in known_tools:
		tool.visible = false
	current_tool.visible = true
	return


# Need to split the _process function into 2:
# one for the editor (so when the editor is active)
# and one for the tool (so tools not being used
# aren't wasting resources updating themselves
# every frame).
func _process(delta: float) -> void:
	if current_tool and current_tool != null:
		current_tool._tool_process(delta)
	_editor_process(delta)
	return


# Defined for being overwritten.
func _editor_process(_delta: float) -> void:
	return
