# This is the base class for anything that is used to modify the state
# of a project within an editor. Basically, anything that can be used
# as an editing mode. Tools are exclusive, and only one can be active
# at a time.
class_name BaseTool extends Control

var editor: BaseEditor
var current_rotation: int = 0
var is_applying_operation: bool = false



func new() -> BaseTool:
	return self


# Change self to active mode.
func select() -> void:
	if editor and editor != null:
		editor._set_tool(self)
	return


func _register_editor(target_editor: BaseEditor) -> void:
	editor = target_editor
	return


# This assigns specific actions to "common functions" tools would
# theoretically. Many tools for instance, can rotate their effects,
# and it's easier to overwrite a rotate function in those tools than
# it is to have this rewritten. It also makes creating common shortcuts
# much easier (like r for the example above).
func _input(event):
	if editor == null or not editor:
		return
	if self != editor.current_tool:
		return
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == MOUSE_BUTTON_RIGHT:
			show_tool_menu()
			return
		if event.button_index == MOUSE_BUTTON_LEFT:
			is_applying_operation = true
			apply_operation()
			return
	if event is InputEventMouseButton and event.is_released():
		if event.button_index == MOUSE_BUTTON_LEFT:
			is_applying_operation = false
	if event.is_action("common_rotate") and event.is_pressed(): 
		current_rotation += 1
		input_rotate()
		return


# Everything below this point is meant to be overwritable functions
# for tools, called from the input function or the editor.


func show_tool_menu() -> void:
	return


func apply_operation() -> void:
	return


func input_rotate() -> void:
	return

func _selected() -> void:
	return


func _unselected() -> void:
	return


func _tool_process(_delta: float) -> void:
	return
